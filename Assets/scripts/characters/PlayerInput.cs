﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    LevelManager lm;
    Rigidbody2D rb;

    [SerializeField]
    float moveSpeed = 3;

    // Start is called before the first frame update
    void Start()
    {
        lm = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rb != null)
        {
            Get_Input();
        }
        
    }

    private void Get_Input()
    {
        float xMove = Input.GetAxis("Horizontal");
        float yMove = Input.GetAxis("Vertical");

        Vector2 moveVector = new Vector2(xMove, yMove);

        rb.MovePosition(new Vector2((transform.position.x + moveVector.x * moveSpeed * Time.deltaTime),
            (transform.position.y + moveVector.y * moveSpeed * Time.deltaTime)));
    }
}
