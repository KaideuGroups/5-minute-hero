﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

    public GameObject heroBar;

    public int energyCount = 0;

    private float scaleFactor = .2f;

    // Start is called before the first frame update
    void Start()
    {
        if (heroBar != null)
        {
            heroBar.transform.localScale = new Vector3(0f, 1, 1);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Add_Hero_Energy()
    {
        if (energyCount < 5)
        {
            energyCount += 1;
            Vector3 newEnergy = new Vector3(scaleFactor, 0, 0);
            heroBar.transform.localScale += newEnergy;
        }

    }

    public void Dmg_Hero_Energy()
    {
        if (energyCount > 0)
        {
            energyCount -= 1;
            Vector3 newEnergy = new Vector3(scaleFactor, 0, 0);
            heroBar.transform.localScale -= newEnergy;
        }

    }
}
