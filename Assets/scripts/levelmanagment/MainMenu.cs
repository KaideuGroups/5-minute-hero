﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public Canvas Main_Menu;
    public Canvas Options_Menu;

    public GameObject AM;

    public GameObject BG_Slider;
    public GameObject FX_Slider;

    // Start is called before the first frame update
    void Start()
    {
        AudioManager am = AM.GetComponent<AudioManager>();

        BG_Slider.GetComponent<Slider>().value = am.Music.volume;
        FX_Slider.GetComponent<Slider>().value = am.Sound_FX.volume;

        Main_Menu.enabled = true;
        Options_Menu.enabled = false;

        if (AM != null)
            am.ChangeBGM(AudioManager.BGM.Menu);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleOptions()
    {
        Main_Menu.enabled = !Main_Menu.enabled;
        Options_Menu.enabled = !Options_Menu.enabled;
    }

}
