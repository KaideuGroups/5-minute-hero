﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    private float P_move_time = 0;

    public enum SoundFX
    {
        P_Move,
        P_Eat,
        P_TakeDmg,
        P_Grow,
        P_Die,
        Attack_Melee,
        Attack_Arrow,
        Attack_Magic,
        E_TakeDmg,
        E_Die,
    }

    public enum BGM
    {
        Menu,
        Normal,
        Hero,
    }


    //This plays all sound tracks - Background Music
    [SerializeField]
    public AudioSource Music;

    //This plays sound FX - Anything in Player and enemy enums.
    [SerializeField]
    public AudioSource Sound_FX;


    //Classes for matching enum keys to audioClip for dictionaries
    [System.Serializable]
    public class Background
    {
        public BGM BG_Music;
        public AudioClip audioClip;
    }

    [System.Serializable]
    public class SoundFX_audio
    {
        public SoundFX Sound;
        public AudioClip audioClip;
    }

    //Lists for populating in inspector
    [SerializeField]
    public List<Background> bg_Music = new List<Background>();
    [SerializeField]
    public List<SoundFX_audio> FX_Audio = new List<SoundFX_audio>();

    //Defining dictionaries where we will store the list values for reference in runtime
    public Dictionary<BGM, AudioClip> bg_lib = new Dictionary<BGM, AudioClip>();
    public Dictionary<SoundFX, AudioClip> FX_lib = new Dictionary<SoundFX, AudioClip>();



    public static AudioManager Instance { get; private set; }

    private void Awake()
    {
        //Prevents multiple instances of this object
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {

        //Initialize sound dictionaries for use.
        for (int i = 0; i != bg_Music.Count; i++)
            bg_lib.Add(bg_Music[i].BG_Music, bg_Music[i].audioClip);

        for (int i = 0; i != FX_Audio.Count; i++)
            FX_lib.Add(FX_Audio[i].Sound, FX_Audio[i].audioClip);
    }

    //Changes background music via enum argument
    public void ChangeBGM(BGM audio)
    {
        
        if (bg_lib.ContainsKey(audio) && Music.clip != bg_lib[audio])
        {
            Music.Stop();
            Music.clip = bg_lib[audio];
            Music.Play();
        }
    }

    //Attempts to play sound from Sound FX list
    public void PlaySound(SoundFX audio)
    {

    }

    //Check to see if the sound requested for playing has a timer before next play
    private bool CanPlaySound(SoundFX audio)
    {
        switch (audio)
        {
            default:
                return true;
            case SoundFX.P_Move:
                float lastTimePlayed = P_move_time;
                float MoveTimer = FX_lib[SoundFX.P_Move].length;
                if (lastTimePlayed + MoveTimer < Time.time)
                {
                    P_move_time = Time.time;
                    return true;
                }
                else { return false; }
        }
    }
}
